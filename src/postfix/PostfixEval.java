package postfix;

public class PostfixEval {

	private static String ops = "+-*/";

	private String input;
	private DynamicStack stack = new DynamicStack();
	private boolean error = false;

	public PostfixEval() {
	}

	public PostfixEval(String input) {
		this.input = input;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public boolean hasError() {
		boolean hasError = error;
		error = false;
		return hasError;
	}

	public int eval() {

		stack.clear();

		for (String token : input.split("\\s+")) {

			if (ops.contains(token)) { // token is an operator

				// get the last two numbers

				int n2 = stack.pop(); 
				int n1 = stack.pop();

				// apply operator and put back onto stack

				if (token.equals("+")) { 
					stack.push(n1 + n2);
				}

				if (token.equals("-")) {
					stack.push(n1 - n2);
				}	

				if (token.equals("*")) {
					stack.push(n1 * n2);
				}

				if (token.equals("/")) {
					stack.push(n1 / n2);
				}

			} else { // token is a number, put it onto stack

				stack.push(Integer.parseInt(token));
			}
		}

		// set error if there�s not exactly one number remaining

		if (stack.size() != 1) error = true;
 
		return stack.pop(); // return last number
	}
}
