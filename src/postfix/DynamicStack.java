package postfix;

import java.util.NoSuchElementException;

public class DynamicStack {
	private Node sp;

	public void push(int data) {
		Node n = new Node(data);
		n.next = sp;
		sp = n;
	}

	public int pop() {
		if (isEmpty()) {
			throw new NoSuchElementException("Stack underflow");
		}
		int poppedData = sp.data;
		sp = sp.next;
		return poppedData;
	}

	public int size() {
		if (isEmpty()) {
			return 0;
		} else {
			int size = 1;
			while (sp.next != null) {
				size++;
				sp = sp.next;
			}
			return size;
		}
	}

	public boolean isEmpty() {
		return sp == null;
	}

	public void clear() {
		sp = null;
	}

	private class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
		}
	}
}