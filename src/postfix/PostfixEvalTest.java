package postfix;

public class PostfixEvalTest {

	public static void main(String[] args) {

		PostfixEval p = new PostfixEval();

		String[] s = new String[6];

		s[0] = "2 3 + 4 +";
		s[1] = "2 3 - 4 *";
		s[2] = "10 11 22 33 - + *";
		s[3] = "1 2 3 + 4 - 5 / -";
		s[4] = "1 2 3 4 - 5 *";
		s[5] = "1 2 3 4 5 + - * /";

		for (String str : s) {
			p.setInput(str);
			System.out.println("input: " + p.getInput());
			System.out.println("= " + p.eval());
			System.out.println("error: " + p.hasError());
			System.out.println();
		}
	}
}

